---
layout: post
title: "Introduction Python in HackerRank"
date: 2021-09-05
tags: [python, hackerrank]
---
🍊 [View in Github](https://github.com/syo2000/HackerRank_Python/blob/main/%F0%9F%8D%8A_Python_Introduction_HackerRank.ipynb) 🍊


## Say "Hello, World!" With Python

**Input Format**

You do not need to read any input in this challenge.

**Output Format**

Print Hello, World! to stdout.

```python
print("Hello, World!")
```

## Arithmetic Operators

**Task**

The provided code stub reads two integers from STDIN, ***a*** and ***b***. Add code to print three lines where:
1.   The first line contains the sum of the two numbers.
2.   The second line contains the difference of the two numbers (first - second).
3. The third line contains the product of the two numbers.

**Input Format**

The first line contains the first integer, ***a***.

The second line contains the second integer, ***b***.

**Output Format**

Print the three lines as explained above.

```python
if __name__ == '__main__':
    a = int(input());
    b = int(input());
    print (a + b);
    print (a - b);
    print (a*b);
#print((a + b), (a - b), (a * b), sep='\n')
```

## Python: Division

**Task**

The provided code stub reads two integers, ***a*** and ***b***, from STDIN.

Add logic to print two lines. The first line should contain the result of integer division,***a//b*** . The second line should contain the result of float division,  ***a/b*** .

No rounding or formatting is necessary.

**Input Format**

The first line contains the first integer, ***a***.

The second line contains the second integer, ***b***.

**Output Format**

Print the two lines as described above.

```python
if __name__ == '__main__':
    a = int(input())
    b = int(input())
    print (a//b);
    print (a/b);
```

## Loops

**Task**

The provided code stub reads and integer, n, from STDIN. For all non-negative integers i< n, print i^2 .

**Input Format**

The first and only line contains the integer, n.

**Output Format**

Print n lines, one corresponding to each i.

```python
if __name__ == '__main__':
    n = int(input())
    for i in range(n):
        print (pow(i,2));
```

## Print Function

The included code stub will read an integer,n , from STDIN.

Without using any string methods, try to print the following:

123...n

Note that "...." represents the consecutive values in between.

**Input Format**

The first line contains an integer n.

**Output Format**

Print the list of integers from 1 through n as a string, without spaces.

```python
if __name__ == '__main__':
    n = int(input())
    for i in range(1, n+1):
        print(i, end="")
```

## Write a function

**Task**

Given a year, determine whether it is a leap year. If it is a leap year, return the Boolean `True`, otherwise return `False`.

Note that the code stub provided reads from STDIN and passes arguments to the `is_leap` function. It is only necessary to complete the `is_leap` function.

[Source](https://www.timeanddate.com/date/leapyear.html)

**Input Format**

Read year, the year to test.

**Output Format**

The function must return a Boolean value (True/False). Output is handled by the provided code stub.

```python
#method 1
import calendar

def is_leap(year):
    return calendar.isleap(year)

year = int(input())
print(is_leap(year))
```

```python
#method 2
def is_leap(year):
    leap = False;
    if year%4 == 0:
        if year%400 == 0:
            leap = true;
        elif year%100 == 0:
            leap =  false;  
    return leap
year = int(input())
print(is_leap(year))
```

```python
#method 3
def is_leap(year):
    return year % 4 == 0 and (year % 400 == 0 or year % 100 != 0)
year = int(input())
print(is_leap(year))
```





